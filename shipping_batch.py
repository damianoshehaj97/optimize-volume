# Part of OBA40. See LICENSE file for full copyright and licensing details.

import math
from odoo import api, fields, models, _
from odoo.exceptions import UserError

from .integration import allocate
# from .main import allocate


class StockPickingBatchJob(models.Model):
    _name = "picking.batch.job"
    _description = "Picking Batch Job"
    _order = "line_ids asc"
    _inherit = ["mail.thread"]

    name = fields.Char(
        string="Batch Job Name",
        default="New",
        copy=False,
        required=True,
        help="Name of the batch job picking",
    )
    picking_ids = fields.One2many(
        "stock.picking",
        "job_batch_id",
        string="Pickings",
        help="List of picking associated to this batch job",
    )
    state = fields.Selection(
        selection=[
            ("new", "New"),
            ("draft", "Draft"),
            ("waiting", "Waiting"),
            ("ready", "Ready"),
            ("done", "Done"),
            ("cancel", "Cancelled"),
        ],
        string="State",
        required=True,
        default="new",
        copy=False,
        track_visibility="onchange",
    )
    planning_line_ids = fields.One2many(
        "picking.batch.planning_line",
        "job_batch_id",
        string="Planning Lines",
        copy=False,
    )
    line_ids = fields.One2many(
        "picking.batch.line", "job_batch_id", string="Shipping Lines", copy=False
    )

    line_count = fields.Integer(compute="_compute_line_count", string="Number of Lines")
    plan_line_count = fields.Integer(
        compute="_compute_line_count", string="Number of Plan Lines"
    )

    def _compute_line_count(self):
        for batch in self:
            batch.line_count = len(batch.line_ids)
            batch.plan_line_count = len(batch.planning_line_ids)

    def create_planning_lines(self):
        if self.planning_line_ids:
            self.planning_line_ids.unlink()
        for partner in self.picking_ids.mapped("partner_id"):
            self.env["picking.batch.planning_line"].create(
                {"partner_id": partner.id, "job_batch_id": self.id}
            )
        self.state = 'draft'

    @api.multi
    def confirm_planning(self):
        if self.line_ids:
            self.line_ids.unlink()

        for line in self.planning_line_ids:
            pallet_spedizione = ([], [])
            total = line.nr_industrial + line.nr_euro
            i = 0
            if line.nr_industrial:
                for x in range(0, line.nr_industrial):
                    i += 1
                    pallet = self.env["stock.quant.package"].create(
                        {
                            "name": "{}-IND:{}/{}-{}".format(
                                line.partner_id.ref or "", i, total, self.name
                            ),
                            "packaging_id": line.industrial_pallet_type.id,
                            "company_id": self.env.user.company_id.id,
                        }
                    )
                    pallet_spedizione[0].append(
                        {
                            "name": "{}-IND:{}/{}-{}".format(
                                line.partner_id.ref or "", i, total, self.name
                            ),
                            "volume": line.industrial_pallet_type.max_weight,
                            "volume_residuo": line.industrial_pallet_type.max_weight,
                            "height": line.industrial_pallet_type.height,
                            "pallet": pallet,
                            "prodotti": [],
                        }
                    )
            if line.nr_euro:
                for x in range(0, line.nr_euro):
                    i += 1
                    pallet = self.env["stock.quant.package"].create(
                        {
                            "name": "{}-EUR:{}/{}-{}".format(
                                line.partner_id.ref or "", i, total, self.name
                            ),
                            "packaging_id": line.euro_pallet_type.id,
                            "company_id": self.env.user.company_id.id,
                        }
                    )
                    pallet_spedizione[1].append(
                        {
                            "name": "{}-EUR:{}/{}-{}".format(
                                line.partner_id.ref or "", i, total, self.name
                            ),
                            "volume": line.euro_pallet_type.max_weight,
                            "volume_residuo": line.euro_pallet_type.max_weight,
                            "height": line.euro_pallet_type.height,
                            "pallet": pallet,
                            "prodotti": [],
                        }
                    )

            product_dict = {}

            for picking in self.picking_ids.filtered(
                    lambda r: r.partner_id == line.partner_id
            ):
                # per i trasformati setto sulla lista in posizione 0 il numero di spazi riservati
                if picking.reserved_space:
                    product_dict.setdefault("TFR", [0])
                    product_dict["TFR"][0] = (
                            product_dict["TFR"][0] + picking.reserved_space
                    )
                for move in picking.move_ids_without_package:
                    # print('MOVE', move.ids)
                    if not move.product_id.volume:
                        continue
                    product_dict.setdefault(move.product_id.id, []).append(move)

            product_new_dict = {}
            # print('PRODUCT DICT', product_dict)
            for product_id, moves in product_dict.items():
                # etichetta speciale per i trasformati, la lista in posizione 0 contiene il valore
                if product_id != "TFR":
                    qty = sum(m.product_uom_qty for m in moves)
                    vol = moves[0].product_id.volume
                    vol_tot = qty * moves[0].product_id.volume
                else:
                    qty = moves[0]
                    vol = 1
                    vol_tot = qty
                product_new_dict[product_id] = {
                    "qty": qty,
                    "height": vol,
                    "moves": moves,
                    "type": "STANDARD"
                }
            pallet_new_dict = {}
            if pallet_spedizione[0]:
                for p0 in pallet_spedizione[0]:
                    pallet_new_dict[p0['pallet'].id] = {
                        'height': p0['height'],
                        'type': 'INDUSTRIAL'
                    }
            if pallet_spedizione[1]:
                for p1 in pallet_spedizione[1]:
                    pallet_new_dict[p1['pallet'].id] = {
                        'height': p1['height'],
                        'type': 'EURO'
                    }

            disp = allocate(pallet_new_dict, product_new_dict)
            # for el in disp:
            #     print(
            #         'Bin ID: {}; Type: {}; Height: {}; Height left {}; Full: {}; Rows filled with boxes: {}; Boxes in last row: {}, Total boxes inside: {}'.format( \
            #             el.id, el.type, el.height, el.available_height, el.is_full, len(el.boxes_inside),
            #             el.boxes_in_last_row, sum([len(x) for x in el.boxes_inside])))
            #     for box_groups in el.boxes_inside:
            #         for box in box_groups:
            #             print(box)
            #     print('\n')



            # def append(pal, prod, split=False):
            #     pal["prodotti"].append(prod)
            #     pal["volume_residuo"] -= prod["vol_tot"]
            #     if split:
            #         pal["volume_residuo"] = 0
            #
            # while product_list:
            #     if pallet_spedizione[0]:
            #         maxind = max(
            #             pallet_spedizione[0], key=lambda x: x["volume_residuo"]
            #         )
            #     else:
            #         maxind = None
            #     if pallet_spedizione[1]:
            #         maxeur = max(
            #             pallet_spedizione[1], key=lambda x: x["volume_residuo"]
            #         )
            #     else:
            #         maxeur = None
            #     maxprod = max(product_list, key=lambda x: x["vol_tot"])
            #
            #     if maxind and (maxprod["vol_tot"] <= maxind["volume_residuo"]):
            #         append(maxind, maxprod)
            #         product_list.pop(product_list.index(maxprod))
            #     elif maxeur and (maxprod["vol_tot"] <= maxeur["volume_residuo"]):
            #         append(maxeur, maxprod)
            #         product_list.pop(product_list.index(maxprod))
            #     elif (
            #         maxind
            #         and maxind["volume_residuo"] > 0
            #         and maxind["volume_residuo"] >= maxprod["vol"]
            #     ):
            #         qty = int(maxind["volume_residuo"] / maxprod["vol"])
            #         vol = maxprod["vol"] * qty
            #         newprod = maxprod.copy()
            #         maxprod["vol_tot"] = maxprod["vol_tot"] - vol
            #         maxprod["qty"] = maxprod["qty"] - qty
            #         newprod["vol_tot"] = vol
            #         newprod["qty"] = qty
            #         append(maxind, newprod, True)
            #     elif (
            #         maxeur
            #         and maxeur["volume_residuo"] > 0
            #         and maxeur["volume_residuo"] >= maxprod["vol"]
            #     ):
            #         # newprod = maxprod.copy()
            #         # maxprod['vol_tot'] = maxprod['vol_tot'] - maxeur['volume_residuo']
            #         # newprod['vol_tot'] = maxeur['volume_residuo']
            #         # append(maxeur,newprod, True)
            #         qty = int(maxeur["volume_residuo"] / maxprod["vol"])
            #         vol = maxprod["vol"] * qty
            #         newprod = maxprod.copy()
            #         maxprod["vol_tot"] = maxprod["vol_tot"] - vol
            #         maxprod["qty"] = maxprod["qty"] - qty
            #         newprod["vol_tot"] = vol
            #         newprod["qty"] = qty
            #         append(maxeur, newprod, True)
            #     else:
            #         raise UserError(
            #             _("Capienza Esaurita sulle pedane %s %s") % (maxind, maxeur)
            #         )
            # print('ALL PALLETS', pallet_new_dict)


            # create_line for main file
            # def create_line(disposition):
            #     for plt in list(disposition):
            #         trasformati = 0
            #         for box_group in plt.boxes_inside:
            #             # for box in box_group:
            #             # prodotto speciale per i trasformati
            #             if box_group[0].product.id == "TFR":
            #                 trasformati += len(box_group)
            #                 continue
            #             self.env["picking.batch.line"].create(
            #                 {
            #                     "job_batch_id": self.id,
            #                     "partner_id": line.partner_id.id,
            #                     "pallet_id": plt.id,
            #                     "product_id": box_group[0].product.id,
            #                     "product_uom_qty": len(box_group),
            #                     "move_line_ids": [(6, 0, [move.id for move in box_group[0].product.moves])],
            #                     "batch_line_planning_id": line.id,
            #                 }
            #             )
            #         if trasformati:
            #             plt.trasformati = trasformati



            # create line for integration file
            def create_line(disposition):
                for plt in disposition:
                    trasformati = 0
                    if 'lista_prodotti' in plt.keys():
                        for box_group in plt['lista_prodotti']:
                            # for box in box_group:
                            # prodotto speciale per i trasformati
                            if box_group['id'] == "TFR":
                                trasformati += box_group['qty']
                                continue
                            self.env["picking.batch.line"].create(
                                {
                                    "job_batch_id": self.id,
                                    "partner_id": line.partner_id.id,
                                    "pallet_id": plt['id'],
                                    "product_id": box_group['id'],
                                    "product_uom_qty": box_group['qty'],
                                    "move_line_ids": [(6, 0, [move.id for move in box_group['moves']])],
                                    "batch_line_planning_id": line.id,
                                }
                            )
                        if trasformati:
                            plt['trasformati'] = trasformati







            # def create_line(pallets):
            #     for plt in pallets:
            #         trasformati = 0
            #         for prodotti in plt["prodotti"]:
            #             # prodotto speciale per i trasformati
            #             if prodotti["product_id"] == "TFR":
            #                 trasformati += prodotti["qty"]
            #                 continue
            #             moves_ids = []
            #             for prod_move in prodotti["moves"]:
            #                 moves_ids = moves_ids + prod_move.ids
            #             self.env["picking.batch.line"].create(
            #                 {
            #                     "job_batch_id": self.id,
            #                     "partner_id": line.partner_id.id,
            #                     "pallet_id": plt["pallet"].id,
            #                     "product_id": prodotti["product_id"],
            #                     "product_uom_qty": prodotti["qty"],
            #                     "move_line_ids": [(6, 0, moves_ids)],
            #                     "batch_line_planning_id": line.id,
            #                 }
            #             )
            #         if trasformati:
            #             plt["pallet"].trasformati = trasformati

            create_line(disposition=disp)
        self.state = "waiting"

    @api.multi
    def produce(self):
        production_to_be_cancel = self.env["mrp.production"].search(
            [("state", "=", "confirmed")]
        )
        for production in production_to_be_cancel:
            production.action_cancel()
            production.unlink()
        for product in self.line_ids.mapped("product_id"):
            production_plan = {}
            lines = self.line_ids.filtered(lambda p: p.product_id == product)
            for line in lines:
                for lot in line.product_lot_ids:
                    if not lot.need_production:
                        continue
                    production_plan.setdefault(
                        lot.product_lot_id, {
                            "qty": 0,
                            "remain": 0,
                            "batch_lots": self.env["picking.batch.line.lots"]
                        }
                    )
                    production_plan[lot.product_lot_id][
                        "qty"
                    ] += lot.product_uom_qty_done
                    production_plan[lot.product_lot_id][
                        "remain"
                    ] += lot.product_uom_bom_qty_remain
                    production_plan[lot.product_lot_id]["batch_lots"] |= lot
            # print("to_be_produce", production_plan)
            # for prod, lot in to_be_produce.items():
            #     print('prod/lot', prod, lot)
            #     production_plan['lot'] = lot
            #     production_plan['qty'] += lot.product_uom_qty_done
            for lot, qta in production_plan.items():
                bom_id = None
                for bom in product.bom_ids:
                    if bom.bom_line_ids[0].product_id == lot.product_id:
                        bom_id = bom.id
                        break
                if not bom_id:
                    raise UserError(
                        _("Errore recupero bom sul prodotto {} con lotto {}".format(product.name, lot.name)))
                production = self.env["mrp.production"].create(
                    {
                        "product_id": product.id,
                        "product_qty": qta["qty"],
                        "product_uom_id": product.uom_id.id,
                        "bom_id": bom_id,
                        "company_id": self.env.user.company_id.id,
                    }
                )
                qty_used = sum(
                    qt.quantity
                    for qt in lot.quant_ids.filtered(
                        lambda q: q.location_id == production.location_src_id
                    )
                )
                qty_used = qty_used - qta["remain"]
                do_produce = (
                    self.env["mrp.product.produce"]
                        .with_context(active_id=production.id)
                        .create({})
                )
                do_produce.product_qty = qta["qty"]
                do_produce._onchange_product_qty()
                lotto_produzione = self.env["stock.production.lot"].create(
                    {
                        "name": "{}-{}".format(lot.name, production.id),
                        "product_id": product.id,
                    }
                )
                do_produce.lot_id = lotto_produzione
                do_produce.produce_line_ids[0].lot_id = lot
                expected = do_produce.produce_line_ids[0].qty_done
                do_produce.produce_line_ids[0].qty_done = qty_used
                do_produce.do_produce()
                for lot_line in qta["batch_lots"]:
                    lot_line.write(
                        {
                            "product_uom_qty_final_done": lot_line.product_uom_qty_done,
                            "final_product_lot_id": lotto_produzione.id
                        }
                    )
                if len(production.finished_move_line_ids) == 2:
                    industria = qty_used - expected
                    if industria > 0:
                        lotto_industria = self.env["stock.production.lot"].create(
                            {
                                "name": "{}-{}".format(lot.name, production.id),
                                "product_id": production.finished_move_line_ids[
                                    1
                                ].product_id.id,
                            }
                        )
                        production.finished_move_line_ids[1].qty_done = (
                            industria if industria > 0 else 0
                        )
                        production.finished_move_line_ids[1].lot_id = lotto_industria
                    else:
                        production.finished_move_line_ids[1].unlink()
                scrap_rate = ((qty_used - expected) / expected) * 100
                lot.scrap_rate = scrap_rate
                lot.product_id.scrap_rate = scrap_rate
                production.button_mark_done()

    @api.multi
    def confirm_picking(self):
        for line in self.line_ids:
            for lot in line.product_lot_ids:
                if lot.done:
                    continue
                qty_lot = lot.product_uom_qty_final_done
                for move in line.move_line_ids:
                    if qty_lot <= 0:
                        break
                    # todo: verificare
                    # qty_move = move.product_uom_qty - move.quantity_done
                    # if qty_move <= 0:
                    #    continue
                    # rimetto originale
                    qty_move = move.product_uom_qty
                    for move_l in move.move_line_ids:
                        if move_l.product_id == lot.product_id and not move_l.qty_done:
                            move_l.unlink()
                        else:
                            qty_move -= move_l.qty_done
                    if qty_move == 0:
                        continue
                    if qty_move < 0:
                        raise UserError(
                            _("Quantita' da spedire minore o uguale a zero durante il calcolo ... controllare i dati"))
                    move_line = self.env["stock.move.line"].create(
                        {
                            'move_id': move.id,
                            'picking_id': move.picking_id.id,
                            'product_id': lot.product_id.id,
                            'product_uom_id': lot.product_id.uom_id.id,
                            'location_id': move.location_id.id,
                            'location_dest_id': move.location_dest_id.id,
                            'lot_id': lot.final_product_lot_id.id,
                            'result_package_id': line.pallet_id.id,
                            'qty_done': min(qty_move, qty_lot),
                        }
                    )
                    move_line.onchange_product_id()
                    # qui la modifica
                    # qty_lot -= min(lot.product_uom_qty_final_done, move.product_uom_qty)
                    # nuova linea
                    qty_lot -= min(qty_move, qty_lot)
                if qty_lot <= 0:
                    lot.done = True
        # self.state = 'ready'

    @api.multi
    def create_ddt(self):
        self.state = "done"

    @api.multi
    def cancel_job(self):
        if self.line_ids:
            self.line_ids.unlink()
        self.state = "draft"

    @api.multi
    def swline_tree_view(self):
        self.ensure_one()
        domain = [("job_batch_id", "=", self.id)]
        return {
            "name": _("Lines"),
            "res_model": "picking.batch.line",
            "type": "ir.actions.act_window",
            "view_id": False,
            "view_mode": "tree,form,kanban",
            "view_type": "form",
            "help": _(
                """<p class="oe_view_nocontent_create">
                        Linee di spedizione.</p>"""
            ),
            "limit": 80,
            "domain": domain,
        }

    @api.multi
    def plan_line_tree_view(self):
        self.ensure_one()
        domain = [("job_batch_id", "=", self.id)]
        return {
            "name": _("Plan Lines"),
            "res_model": "picking.batch.planning_line",
            "type": "ir.actions.act_window",
            "view_id": False,
            "view_mode": "tree,form,kanban",
            "view_type": "form",
            "help": _(
                """<p class="oe_view_nocontent_create">
                        Linee di spedizione.</p>"""
            ),
            "limit": 80,
            "domain": domain,
        }


class ShippingWavePalanningLine(models.Model):
    _name = "picking.batch.planning_line"
    _description = "Batch Planning Line"

    job_batch_id = fields.Many2one(
        "picking.batch.job", string="Stock Batch Picking Job", ondelete="cascade"
    )
    partner_id = fields.Many2one("res.partner", string="Destination of Goods")
    country_id = fields.Many2one(related="partner_id.country_id", store=True)
    city = fields.Char(related="partner_id.city", store=True)
    zip = fields.Char(related="partner_id.zip", store=True)
    euro_pallet_type = fields.Many2one("product.packaging", string="Euro Type")
    industrial_pallet_type = fields.Many2one(
        "product.packaging", string="Industrial Type"
    )
    volume = fields.Float("Total Vol.", compute="_product_volume", store=True)
    weight = fields.Float("Peso")
    nr_euro = fields.Integer("Euro Pallets", default=0)
    nr_industrial = fields.Integer("Industrial Pallets", default=0)
    # molti a molti
    primary_carrier_id = fields.Many2one("delivery.carrier", string="Primary Carrier")
    other_carrier_ids = fields.Many2many("delivery.carrier", string="Other Carrier")

    def _make_box(self, volume, industrial=True):

        cind = self.industrial_pallet_type.max_weight if self.industrial_pallet_type else None
        ceurobase = self.euro_pallet_type.max_weight if self.euro_pallet_type else None

        if not cind and not ceurobase:
            return [0, 0]
        elif cind and not ceurobase:
            return [0, math.ceil(volume / cind)]
        ceuro = math.ceil(volume / ceurobase)
        if not cind:
            return [ceuro, 0]

        combinations = []
        for x in range(0, 4):
            for y in range(0, ceuro + 1):
                combinations.append([y, x])

        guess_comb = None
        comb = None
        for combinazion in combinations:
            max_space = combinazion[0] * ceurobase + combinazion[1] * cind
            if max_space >= volume:
                guess_comb = min(guess_comb, max_space) if guess_comb else max_space
                if guess_comb == max_space:
                    comb = combinazion
        return comb

    @api.depends("job_batch_id.picking_ids")
    def _product_volume(self):
        for line in self:
            volume = 0
            for picking in line.job_batch_id.picking_ids.filtered(
                    lambda r: r.partner_id == line.partner_id
            ):
                for move in picking.move_ids_without_package:
                    if not move.product_id.volume:
                        continue
                    volume += move.product_uom_qty * move.product_id.volume
                if picking.reserved_space:
                    volume += picking.reserved_space
            line.volume = volume

    @api.onchange("euro_pallet_type", "industrial_pallet_type")
    def on_change_pallet_type(self):
        if not self.euro_pallet_type and not self.industrial_pallet_type:
            return
        comb = self._make_box(
            self.volume, industrial=self.industrial_pallet_type or False
        )
        self.nr_euro = comb[0]
        self.nr_industrial = comb[1]


class ShippingBatchLine(models.Model):
    _name = "picking.batch.line"
    _description = "Shipping batch Line"

    job_batch_id = fields.Many2one(
        "picking.batch.job", string="Batch Shipping", ondelete="cascade", required=True
    )
    batch_line_planning_id = fields.Many2one(
        "picking.batch.planning_line", string="Planning Line", ondelete="cascade"
    )
    partner_id = fields.Many2one("res.partner", string="Partner")
    pallet_id = fields.Many2one(
        "stock.quant.package", string="Pallet", ondelete="cascade"
    )
    product_id = fields.Many2one("product.product", string="Product")
    product_bom_ids = fields.Many2many(
        "product.product", compute="_compute_bom_product", string="Bom Products"
    )
    product_uom_qty = fields.Float("Product Quantity")
    product_uom_done = fields.Float(
        "Product Done", compute="_compute_qty_done", store=True
    )
    product_lot_ids = fields.One2many(
        "picking.batch.line.lots",
        "batch_line_id",
        string="Lots",
        help="List of lots associated to this line",
    )
    move_line_ids = fields.Many2many("stock.move", string="Stock Moves")

    @api.depends("product_lot_ids")
    def _compute_qty_done(self):
        for line in self:
            line.product_uom_done = sum(
                lot.product_uom_qty_final_done or lot.product_uom_qty_done
                for lot in line.product_lot_ids
            )

    @api.depends("product_id")
    def _compute_bom_product(self):
        for line in self:
            for bom in line.product_id.bom_ids:
                if bom.bom_line_ids and bom.product_id == line.product_id:
                    line.product_bom_ids |= bom.bom_line_ids[0].product_id

    def action_show_details_lots(self):
        """ Returns an action that will open a form view (in a popup) allowing to work on all the
        move lines of a particular move. This form view is used when "show operations" is not
        checked on the picking type.
        """
        self.ensure_one()

        # If "show suggestions" is not checked on the picking type, we have to filter out the
        # reserved move lines. We do this by displaying `move_line_nosuggest_ids`. We use
        # different views to display one field or another so that the webclient doesn't have to
        # fetch both.

        view = self.env.ref(
            "legallinefelici_customization.view_shipping_wave_line_lots_operations_lots"
        )

        return {
            "name": _("Detailed Operations"),
            "type": "ir.actions.act_window",
            "view_type": "form",
            "view_mode": "form",
            "res_model": "picking.batch.line",
            "views": [(view.id, "form")],
            "view_id": view.id,
            "target": "new",
            "res_id": self.id,
        }

    def action_show_details_productions(self):
        """ Returns an action that will open a form view (in a popup) allowing to work on all the
        move lines of a particular move. This form view is used when "show operations" is not
        checked on the picking type.
        """
        self.ensure_one()

        # If "show suggestions" is not checked on the picking type, we have to filter out the
        # reserved move lines. We do this by displaying `move_line_nosuggest_ids`. We use
        # different views to display one field or another so that the webclient doesn't have to
        # fetch both.

        view = self.env.ref(
            "legallinefelici_customization.view_shipping_wave_line_lots_operations_prods"
        )

        return {
            "name": _("Detailed Operations"),
            "type": "ir.actions.act_window",
            "view_type": "form",
            "view_mode": "form",
            "res_model": "picking.batch.line",
            "views": [(view.id, "form")],
            "view_id": view.id,
            "target": "new",
            "res_id": self.id,
        }


class ShippingWaveLineLots(models.Model):
    _name = "picking.batch.line.lots"
    _description = "Shipping Wave Line Lots"

    batch_line_id = fields.Many2one(
        "picking.batch.line", string="batch Line", ondelete="cascade", required=True
    )
    product_id = fields.Many2one(
        string="Base Product", related="batch_line_id.product_id"
    )
    product_bom_ids = fields.Many2many(
        "product.product", compute="_compute_products_bom", string="Bom Product"
    )
    product_lot_id = fields.Many2one(
        "stock.production.lot",
        string="Product Lot",
        ondelete="cascade",
        required=False,
        domain="[('product_id', 'in', product_bom_ids)]",
    )
    product_uom_qty_done = fields.Float("Quantity Done", required=False)
    product_uom_bom_qty_remain = fields.Float("Quantity Remain", required=False)
    final_product_lot_id = fields.Many2one(
        "stock.production.lot",
        string="Product Lot",
        ondelete="cascade",
        required=False,
        domain="[('product_id', '=', product_id)]",
    )
    product_uom_qty_final_done = fields.Float("Quantity Final Done", required=False)
    need_production = fields.Boolean("need production", compute="_compute_production_request")
    done = fields.Boolean("Lot Done", default=False)

    @api.depends("product_uom_qty_done")
    def _compute_production_request(self):
        for lot in self:
            if lot.product_uom_qty_done and not lot.product_uom_qty_final_done:
                lot.need_production = True

    @api.depends("batch_line_id")
    def _compute_products_bom(self):
        for lots in self:
            if lots.batch_line_id:
                lots.product_bom_ids |= lots.batch_line_id.product_bom_ids
