import json
from collections import defaultdict
from pprint import pprint


h27 = {"type": "h27", "height": 27, "base": 1}
h20 = {"type": "h20", "height": 20, "base": 2}
h15 = {"type": "h15", "height": 15, "base": 1}
h12 = {"type": "h12", "height": 12, "base": 1}
box_types = [h27, h20, h15, h12]

b1 = {"base": 8}
b2 = {"base": 10}

def get_pallet_base(pallet_type):
    if pallet_type == "INDUSTRIAL":
        return b2
    else:
        return b1

def get_box_for_product(product_height):
    if 0 < product_height <= 0.44:
        return h27
    elif 0.44 < product_height <= 0.57:
        return h20
    elif 0.57 < product_height <= 0.78:
        return h15
    elif 0.78 < product_height <= 2:
        return h12
    else:
        return None


def allocate(pallet_dict, product_dict):
    # crate list of pallets
    list_of_pallets = []
    for pallet_id, pallet_details in pallet_dict.items():
        list_of_pallets.append({'id':pallet_id, 'type':get_pallet_base(pallet_details['type']), 'height': pallet_details['height']})
    # create list of products
    list_of_products = []
    for product_id, product_details in product_dict.items():
        list_of_products.append({'id':product_id, 'type':get_box_for_product(product_details['height']), 'qty':product_details['qty'], 'moves':product_details['moves']})

    for bin in list_of_pallets:
        for box in box_types:
            if box["height"] == 27:
                bin["h27"] = int(bin["height"] / 100 * bin["type"]["base"] * 2)
            elif box["height"] == 20:
                bin["h20"] = int(bin["height"] / 100 * bin["type"]["base"])
            elif box["height"] == 15:
                bin["h15"] = int(bin["height"] / 100 * bin["type"]["base"] * 2)
            else:
                bin["h12"] = int(bin["height"] / 100 * bin["type"]["base"] * 2)

    dict_tot_prodotti = {}
    for p in list_of_products:
        dict_tot_prodotti.setdefault(p["type"]["type"], 0)
        dict_tot_prodotti[p["type"]["type"]] += p["qty"]
    tot_prodotti = 0
    for tkey, tvalue in dict_tot_prodotti.items():
        #print(tkey)
        if tkey == "h20":
            #print(tvalue)
            tot_prodotti += tvalue
        else:
            #print(tvalue / 2)
            tot_prodotti += int(tvalue / 2)
            #if tvalue - int(tvalue) == 0.5:
            #    tot_prodotti += 1
    max_prodotti = 0
    for b in list_of_pallets:
        max_prodotti += b["h20"]  # l'unità base è il box di tipo h20
    print(tot_prodotti)
    print(max_prodotti)
    coeff_di_riempimento = tot_prodotti / max_prodotti
    # Calcolo spazi livellati disponibili per bin
    for bin in list_of_pallets:
        bin["d_h27"] = int(bin["h27"] * coeff_di_riempimento)
        bin["d_h20"] = int(bin["h20"] * coeff_di_riempimento)
        bin["d_h15"] = int(bin["h15"] * coeff_di_riempimento)
        bin["d_h12"] = int(bin["h12"] * coeff_di_riempimento)
    pprint(list_of_pallets)
    # TODO: GESTIRE LO SCARTO
    ##################################################
    print("START")
    loop = 100
    while list_of_products and loop:
        loop -= 1
        # estraree il max prodotto
        max_prodotto = max(list_of_products, key=lambda x: x["qty"])
        # estrarre il bin con capienza max per stessa tipologia di box
        max_bin = max(list_of_pallets, key=lambda x: x["d_" + max_prodotto["type"]["type"]])
        max_bin.setdefault("lista_prodotti", [])
        qty_to_add = min(max_prodotto["qty"], max_bin["d_" + max_prodotto["type"]["type"]])
        if qty_to_add <= 0:
            # distribuire lo scarto su un bin dove già c'è lo stesso prodotto con minore quantitativo
            #print("max_prodotto ", max_prodotto)
            list_of_new_bins = []
            for bin in list_of_pallets:
                # distribuire lo scarto dove c'è spazio sul bin
                if bin[max_prodotto["type"]["type"]] >= max_prodotto["qty"]:
                    tot_prod = 0
                    for prod in bin['lista_prodotti']:
                        if prod['id'] == max_prodotto['id']:
                            #print("id ", prod['id'])
                            tot_prod += prod['qty']
                    if tot_prod > 0:
                        list_of_new_bins.append((bin, tot_prod))
            #print("list_of_new_bins ", list_of_new_bins)
            if list_of_new_bins:
                new_bin = min(list_of_new_bins, key=lambda x: x[1])[0]
                print("new_bin ", new_bin)
                copy_prodotto = max_prodotto.copy()
                copy_prodotto["qty"] = max_prodotto["qty"]
                new_bin["lista_prodotti"].append(copy_prodotto)
                # riduzione applicata a tutte le tipologie di box
                coeff_reduce = 1 - (qty_to_add / new_bin[max_prodotto["type"]["type"]])
                new_bin["h27"] = int(new_bin["h27"] * coeff_reduce)
                new_bin["h20"] = int(new_bin["h20"] * coeff_reduce)
                new_bin["h15"] = int(new_bin["h15"] * coeff_reduce)
                new_bin["h12"] = int(new_bin["h12"] * coeff_reduce)
                max_prodotto["qty"] -= qty_to_add
                # in caso di qty a zero per il tipo prodotto attuale lo rimuovo
                list_of_products.remove(max_prodotto)
                continue
            else:
                continue
        copy_prodotto = max_prodotto.copy()
        copy_prodotto["qty"] = qty_to_add
        max_bin["lista_prodotti"].append(copy_prodotto)
        # riduzione applicata a tutte le tipologie di box
        coeff_reduce = 1 - (qty_to_add / max_bin["d_" + max_prodotto["type"]["type"]])
        remaining = int(max_bin["d_h27"] * coeff_reduce)
        max_bin["h27"] -= (max_bin["d_h27"] - remaining)
        max_bin["d_h27"] = remaining
        remaining = int(max_bin["d_h20"] * coeff_reduce)
        max_bin["h20"] -= (max_bin["d_h20"] - remaining)
        max_bin["d_h20"] = remaining
        remaining = int(max_bin["d_h15"] * coeff_reduce)
        max_bin["h15"] -= (max_bin["d_h15"] - remaining)
        max_bin["d_h15"] = remaining
        remaining = int(max_bin["d_h12"] * coeff_reduce)
        max_bin["h12"] -= (max_bin["d_h12"] - remaining)
        max_bin["d_h12"] = remaining
        max_prodotto["qty"] -= qty_to_add
        # in caso di qty a zero per il tipo prodotto attuale lo rimuovo
        if max_prodotto["qty"] <= 0:
            list_of_products.remove(max_prodotto)
    print("loop ", loop)
    print("list_of_products ", list_of_products)
    ##################################################################################
    # verifica qty
    qty = defaultdict(int)
    for bin in list_of_pallets:
        if "lista_prodotti" in bin.keys():
            for prodotti in bin["lista_prodotti"]:
                qty[prodotti["type"]["type"]] += prodotti["qty"]
    # pprint(dict_tot_prodotti)
    # pprint(qty)

    for bin in list_of_pallets:
        prod_occurrencies = defaultdict(list)
        for prod in bin['lista_prodotti']:
            prod_occurrencies[prod['id']].append(prod)
        for prod_found_id, prods_list in prod_occurrencies.items():
            if len(prods_list) > 1:
                tot_qty_for_prod = sum([prod['qty'] for prod in prods_list])
                for prod in prods_list:
                    bin['lista_prodotti'].remove(prod)
                new_prod_entry = prods_list[0]
                new_prod_entry['qty'] = tot_qty_for_prod
                bin['lista_prodotti'].append(new_prod_entry)
    # pprint(dict_tot_prodotti)
    # pprint(qty)




    return list_of_pallets


if __name__ == '__main__':
    with open('test_bins_v2.json') as json_bins:
        dict_of_bins = json.load(json_bins)
    with open('test_products_v2.json') as json_products:
        dict_of_products = json.load(json_products)

    d = allocate(dict_of_bins, dict_of_products)
    print("END")
    pprint(d)
